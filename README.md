# PBguetzli Script - README #
---

### Overview ###

The **PBguetzli** script (Batch) will optimize JPG and PNG images. The filesize of the JPG images will be reduced and the PNG images will also be converted to the JPG format. There are different optimizing/quality levels available ranging from 84% to 100%. The optimizing script uses the [**Guetzli JPEG encoder**](https://github.com/google/guetzli) which aims for excellent compression density at high visual quality. Guetzli generates only sequential (nonprogressive) JPEGs due to faster decompression speeds they offer. You can read more about Guetzli on the the following GitHub website:

GitHub: [**Guetzli - Perceptual JPEG encoder**](https://github.com/google/guetzli)

If the optimization process has to be faster, the [**Mozjpeg encoder**](https://github.com/mozilla/mozjpeg) can be used alternatively. The optimization process will be faster but maybe the quality/size ratio will be less optimal. You can read more about Mozjpeg on the the following GitHub website:

GitHub: [**Mozilla - JPEG Encoder Project**](https://github.com/mozilla/mozjpeg)

To display a colored output, this script uses an enhanced ECHO command line utility with color support from Thomas Polaert. A copy of the [**The Code Project Open License (CPOL)**](https://www.codeproject.com/info/cpol10.aspx) is provided in the license directory of this optimizer. To get more information about this nice tool you can visit the following Code Project website:

Code Project: [**Add Colors to Batch Files**](https://www.codeproject.com/Articles/17033/Add-Colors-to-Batch-Files)

### Screenshots ###

![PBguetzli - Guetzli Optimizing Process](development/readme/pbguetzli_1.png "PBguetzli - Guetzli Optimizing Process")
![PBguetzli - Guetzli Optimizing Result](development/readme/pbguetzli_2.png "PBguetzli - Guetzli Optimizing Result")
![PBguetzli - Mozjpeg Optimizing Process](development/readme/pbguetzli_3.png "PBguetzli - Mozjpeg Optimizing Process")
![PBguetzli - Mozjpeg Optimizing Result](development/readme/pbguetzli_4.png "PBguetzli - Mozjpeg Optimizing Result")

### Setup ###

* Copy the directory **pbguetzli** to your Windows computer.
* Copy your image files (JPG/PNG) into the directory **pbguetzli\input**.
* Doubleclick on a **pbguetzli\Start_encoder_quality.bat** file.
* Example: Choose 'pbguetzli\Start_guetzli_84.bat' for optimizing with Guezli with an image quality of 84.
* Example: Choose 'pbguetzli\Start_mozjpeg_70.bat' for optimizing with Mozjpeg with an image quality of 70.
* The **PBguetzli Script** will optimize your files recursively.
* The script will display an error if one of the input files has not a valid format.
* The optimized files will be placed in the directory **pbguetzli\output**.
* The original files remain intact and will not be changed.
* As default the **PBguetzli Script** uses the 64-Bit version of the **Guetzli** encoder.
* As default the **PBguetzli Script** uses the 32-Bit version of the **Mozjpeg** encoder.

### Support ###

This is a free tool and support is not included and guaranteed. Nevertheless I will try to answer all your questions if possible. So write to my email address **biegel[at]gmx.ch** if you have a question :-)

### License ###

The **PBguetzli** script is licensed under the [**MIT License (Expat)**](https://pb-soft.com/resources/mit_license/license.html) which is published on the official site of the [**Open Source Initiative**](https://opensource.org/licenses/MIT).
