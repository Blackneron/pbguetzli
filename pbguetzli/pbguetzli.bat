@ECHO off

REM ####################################################################
REM #                                                                  #
REM #                                                                  #
REM #      P B G U E T Z L I   -   I M A G E   O P T I M I Z E R       #
REM #                                                                  #
REM #            Copyright 2018 by PB-Soft / Patrick Biegel            #
REM #                                                                  #
REM #                       https://pb-soft.com                        #
REM #                                                                  #
REM ####################################################################
REM #                                                                  #
REM #                PBguetzli version 2.3 / 03.06.2018                #
REM #                                                                  #
REM #                Guetzli version 1.0.1 / 23.12.2017                #
REM #                                                                  #
REM #                Mozjpeg version 3.1.0 / 04.09.2015                #
REM #                                                                  #
REM ####################################################################
REM #                                                                  #
REM #       Guetzli Website: https://github.com/google/guetzli         #
REM #                                                                  #
REM #       Mozjpeg Website: https://github.com/mozilla/mozjpeg        #
REM #                                                                  #
REM ####################################################################
REM #                                                                  #
REM #  This software is based in part on the work of the Independent   #
REM #  JPEG Group (Mozjpeg), Google LLC (Guetzli) and Thomas Polaert   #
REM #  (colored output). Thanks to all of them for making their soft-  #
REM #  ware available for all!                                         #
REM #                                                                  #
REM ####################################################################
REM #                                                                  #
REM #  C O L O R   S U P P O R T                                       #
REM #                                                                  #
REM #  To display a colored output, this script uses an enhanced ECHO  #
REM #  command line utility with color support from Thomas Polaert. A  #
REM #  copy of the 'The Code Project Open License (CPOL)' is provided  #
REM #  in the license directory of this optimizer. To get more infor-  #
REM #  mation about this nice tool you can visit the following 'Code   #
REM #  Project' website:                                               #
REM #                                                                  #
REM #  www.codeproject.com/Articles/17033/Add-Colors-to-Batch-Files    #
REM #                                                                  #
REM ####################################################################
REM #                                                                  #
REM #  C H A N G E S :                                                 #
REM #                                                                  #
REM #  Version 2.3                                                     #
REM #                                                                  #
REM #  - Batch integer overflow problems fixed.                        #
REM #  - The 'Mozjpeg' method was added for faster results.            #
REM #  - Small design changes (output).                                #
REM #                                                                  #
REM #  Version 2.2                                                     #
REM #                                                                  #
REM #  - File copy/delete problem fixed.                               #
REM #  - Check identifier file before deleting output content.         #
REM #  - Code optimized after tests with > 6000 files.                 #
REM #                                                                  #
REM #  Version 2.1                                                     #
REM #                                                                  #
REM #  - Better error handling.                                        #
REM #  - Cosmetic changes to the script output/colors.                 #
REM #                                                                  #
REM #  Version 2.0                                                     #
REM #                                                                  #
REM #  - Recursive processing of JPG / JPEG / PNG images.              #
REM #  - Colored output added for a better (success/fail) overview.    #
REM #  - Log errors to logfile 'error.log'.                            #
REM #                                                                  #
REM #  Version 1.0                                                     #
REM #                                                                  #
REM #  - Processing of JPG / JPEG / PNG images in only one folder.     #
REM #                                                                  #
REM ####################################################################


REM ####################################################################
REM ####################################################################
REM ###                                                              ###
REM ###   C O N F I G U R A T I O N   S E C T I O N   -   B E G I N  ###
REM ###                                                              ###
REM ####################################################################
REM ####################################################################


REM ====================================================================
REM                       S C R E E N   C O L O R
REM ====================================================================
REM
REM Please specify the background/foreground color. The first digit is
REM the background color and the second one the foreground color.
REM
REM   0 = Black     8 = Gray
REM   1 = Blue      9 = Light Blue
REM   2 = Green     A = Light Green
REM   3 = Aqua      B = Light Aqua
REM   4 = Red       C = Light Red
REM   5 = Purple    D = Light Purple
REM   6 = Yellow    E = Light Yellow
REM   7 = White     F = Bright White
REM
REM Example: COLOR=18 (Gray text on blue background)
REM
REM ====================================================================
COLOR 18


REM ====================================================================
REM    O P T I M I Z A T I O N   M E T H O D
REM ====================================================================
REM
REM Please specify the optimization method. For optimal compression and
REM image quality choose the 'Guetzli' encoder. If you need a faster
REM processing speed but a little bit less optimal image quality then
REM choose the 'Mozjpeg' encoder.
REM
REM   1 = Guetzli encoder
REM   2 = Mozjpeg encoder
REM
REM Example: METHOD=1
REM
REM IMPORTANT: Disable this setting if the script is started by another
REM            script which already specified the optimization method!
REM
REM ====================================================================
REM SET METHOD=1


REM ====================================================================
REM    O P T I M I Z A T I O N   Q U A L I T Y
REM ====================================================================
REM
REM Please specify the quality of the output images. It is possible to
REM choose a quality setting between 84 and 100 for the Guetzli encoder
REM and it is not possible to set a lower quality for using Guetzli. If
REM you use the Mozjpeg encoder you can use settings between 0 and 100.
REM
REM Example: QUALITY=85
REM
REM IMPORTANT: Disable this setting if the script is started by another
REM            script which already specified the image quality!
REM
REM ====================================================================
REM SET QUALITY=85


REM ====================================================================
REM    P N G   O P T I M I Z A T I O N
REM ====================================================================
REM
REM Please specify if the PNG files should be optimized and converted
REM to JPG files (1 = yes / 0 = No). This setting is only used if the
REM 'Guetzli' method was selected for optimization.
REM
REM Example: OPTIMIZE_PNG=1
REM
REM ====================================================================
SET OPTIMIZE_PNG=1


REM ====================================================================
REM    I N P U T   D I R E C T O R Y
REM ====================================================================
REM
REM Please specify the input directory (relative path).
REM
REM Example: DATA_INPUT=input
REM
REM ====================================================================
SET DATA_INPUT=input


REM ====================================================================
REM    O U T P U T   D I R E C T O R Y
REM ====================================================================
REM
REM Please specify the output directory (relative path).
REM
REM Example: DATA_OUTPUT=output
REM
REM ====================================================================
SET DATA_OUTPUT=output


REM ====================================================================
REM    F I L E   E X T E N S I O N
REM ====================================================================
REM
REM Please specify the file extension of the optimized files (jpg/jpeg).
REM If a file is optimized, it will be saved with this file extension.
REM
REM Example: NEW_FILE_EXTENSION=jpg
REM
REM ====================================================================
SET NEW_FILE_EXTENSION=jpg


REM ====================================================================
REM    M I N I M U M   O P T I M I Z A T I O N
REM ====================================================================
REM
REM Please specify the minimum optimization of an image in percent. If
REM the optimizing process can not get this optimization, the file will
REM only be copied to the output directory without any modifications.
REM
REM Example: MIN_OPTIMIZATION=1
REM
REM ====================================================================
SET MIN_OPTIMIZATION=1


REM ====================================================================
REM    M A X I M U M   O P T I M I Z A T I O N
REM ====================================================================
REM
REM Please specify the maximum optimization of an image in percent. If
REM the optimizing value is higher, the file will only be copied to the
REM output directory without any modifications.
REM
REM Example: MAX_OPTIMIZATION=100
REM
REM ====================================================================
SET MAX_OPTIMIZATION=100


REM ====================================================================
REM    L O G   E R R O R S
REM ====================================================================
REM
REM Please specify if the errors should be logged (1 = yes / 0 = No).
REM The errors will be loged into the file 'error.log' in the script
REM directory and the logfile will be overwritten when the next image
REM optimizing process is running.
REM
REM Example: LOG_ERRORS=1
REM
REM ====================================================================
SET LOG_ERRORS=1


REM ####################################################################
REM ####################################################################
REM ###                                                              ###
REM ###     C O N F I G U R A T I O N   S E C T I O N   -   E N D    ###
REM ###                                                              ###
REM ####################################################################
REM ####################################################################


REM ====================================================================
REM    C R E A T E   D I R E C T O R I E S
REM ====================================================================

REM Create input and output directories if necessary.
IF NOT EXIST %DATA_INPUT%\NUL MD %DATA_INPUT%
IF NOT EXIST %DATA_OUTPUT%\NUL MD %DATA_OUTPUT%


REM ====================================================================
REM    E M P T Y   D I R E C T O R Y   C O N T E N T
REM ====================================================================

REM Check if the output identifier file exists.
IF EXIST "%DATA_OUTPUT%\PBGUETZLI_FLAG" (

  REM Empty the output directory if necessary.
  IF EXIST "%DATA_OUTPUT%\*.*" (

    REM Delete all the files recursively.
    DEL /s /f /q "%DATA_OUTPUT%\*.*" > NUL

    REM Delete all empty directories recursively.
    FOR /f %%F IN ('DIR /ad /b %DATA_OUTPUT%\*.*') DO (RD "%DATA_OUTPUT%\%%F" /q /s > NUL)
  )

  REM Create a new output identifier file.
  TYPE NUL > "%DATA_OUTPUT%\PBGUETZLI_FLAG"
)


REM ====================================================================
REM    L O G F I L E   P R E P A R A T I O N
REM ====================================================================

REM Check if a new logfile should be created.
IF %LOG_ERRORS%==1 (

  REM Create the header of the new error logfile.
  ECHO ================================================================================ > error.log
  ECHO. >> error.log
  ECHO       P B G U E T Z L I   -   I M A G E   O P T I M I Z E R   S C R I P T >> error.log
  ECHO. >> error.log
  ECHO                            E R R O R   L O G F I L E >> error.log
  ECHO. >> error.log
  ECHO ================================================================================ >> error.log
  ECHO. >> error.log
)


REM ====================================================================
REM    S P E C I F Y   D I S P L A Y   M E T H O D
REM ====================================================================

REM Check if the optimizing method is 'Guetzli'.
IF %METHOD% EQU 1 (

  REM Specify the display method.
  SET DISPLAY_METHOD=Guetzli

  REM Specify the hint.
  SET HINT=Please be patient and go on vacation... :-^)
)

REM Check if the optimizing method is 'Mozjpeg'.
IF %METHOD% EQU 2 (

  REM Specify the display method.
  SET DISPLAY_METHOD=Mozjpeg

  REM Specify the hint.
  SET HINT=Please be patient and get a coffee... :-^)
)


REM ====================================================================
REM     O P T I M I Z E   I M A G E S   -   J P G / J P E G / P N G
REM ====================================================================

REM Display some information about the script and optimization process.
CLS
ECHO ================================================================================
ECHO.
ECHO       P B G U E T Z L I   -   I M A G E   O P T I M I Z E R   S C R I P T
ECHO.
ECHO                   Copyright 2018 by PB-Soft / Patrick Biegel
ECHO.
ECHO ================================================================================
ECHO.
ECHO  To optimize the filesize of JPG/JPEG and PNG files, please copy the image
ECHO  files to the subdirectory "%DATA_INPUT%" and run this script. Later you can get the
ECHO  optimized image files from the subdirectory "%DATA_OUTPUT%". The PNG files also will
ECHO  be converted to the JPG format. If a file can not be optimized more than the
ECHO  specified minimum or the filesize is not smaller, the original file will only
ECHO  be copied to the output directory without any modifications.
ECHO.
ECHO  Selected optimizing method:          %METHOD% (%DISPLAY_METHOD%)
ECHO.
ECHO  Optimizing all images with quality:  %QUALITY%
ECHO.
ECHO  The minimum optimization is set to:  %MIN_OPTIMIZATION% %%
ECHO.
ECHO  The maximum optimization is set to:  %MAX_OPTIMIZATION% %%
ECHO.
ECHO  %HINT%
ECHO.
ECHO --------------------------------------------------------------------------------

REM Specify the directory of the executables (guetzli.exe / cecho.exe).
SET EXEC_DIR=%CD%

REM Change the actual directory to the input directory.
cd %DATA_INPUT%

REM Start the optimization process (calling the function 'optimizeImages').
call :optimizeImages

REM Goto the end of the script/file.
goto :eof


REM ====================================================================
REM     R E C U R S I V E   O P T I M I Z I N G   F U N C T I O N
REM ====================================================================

REM Function to optimize the images.
:optimizeImages

REM Display a header for the image optimization.
ECHO  Optimizing Images [ Method: %METHOD% ^| Quality: %QUALITY% ^| Minimum: %MIN_OPTIMIZATION% %% ^| Maximum: %MAX_OPTIMIZATION% %% ]
ECHO --------------------------------------------------------------------------------
ECHO.

REM Enable 'DelayedExpansion' to expanded variables at execution time.
SETLOCAL enabledelayedexpansion

REM The following hack is used to insert linebreaks when using the
REM 'DelayedExpansion'. Just insert !\n! for a linebreak. More infos:
REM https://stackoverflow.com/questions/132799/how-can-you-echo-a-newline-in-batch-files/269819#269819
(set \n=^
%=Do not remove this line=%
)

REM Initialize the number of files counters.
SET FILES_TOTAL=0
SET FILES_OPTIMIZED=0
SET FILES_FAILED=0

REM Initialize the total file size counters.
SET TOTAL_SIZE_BEGIN_B=0
SET TOTAL_SIZE_END_B=0

REM Check if the optimizing method is 'Mozjpeg'.
IF !METHOD! EQU 2 (

  REM Disable the PNG optimization.
  SET OPTIMIZE_PNG=0
)

REM Check if the PNG optimization is enabled.
IF %OPTIMIZE_PNG%==1 (

  REM Specify the file extensions of the images.
  SET EXTENSIONS=*.jpg *.jpeg *.png

  REM The PNG optimization is disabled.
) ELSE (

  REM Specify the file extensions of the images.
  SET EXTENSIONS=*.jpg *.jpeg
)

REM Loop through the image files.
FOR /R %%F IN (%EXTENSIONS%) DO (

  REM Initialize the error counter.
  SET ERROR_COUNTER=0

  REM Increase the total file counter.
  SET /a FILES_TOTAL=!FILES_TOTAL!+1

  REM Initialize the input file extension.
  SET INPUT_FILE_EXTENSION="UNKNOWN"

  REM Set the input file extension to display.
  IF /I "%%~xF"==".jpg" (SET INPUT_FILE_EXTENSION=JPG)
  IF /I "%%~xF"==".jpeg" (SET INPUT_FILE_EXTENSION=JPEG)
  IF /I "%%~xF"==".png" (SET INPUT_FILE_EXTENSION=PNG)

  REM Set the output file extension to display.
  IF /I "%NEW_FILE_EXTENSION%"=="jpg" (SET OUTPUT_FILE_EXTENSION=JPG)
  IF /I "%NEW_FILE_EXTENSION%"=="jpeg" (SET OUTPUT_FILE_EXTENSION=JPEG)

  REM Calculate the filesize for the actual file - Before optimizing.
  SET SIZE_BEGIN_B=%%~zF

  REM Display an information message - Input file.
  ECHO  !FILES_TOTAL! - !INPUT_FILE_EXTENSION! - Optimizing input file: %%F  ( !SIZE_BEGIN_B! B ^^^)...

  REM Specify the new input directory because of the recursivity.
  SET INPUT_DIR=%%~dpF

  REM Specify the new output directory because of the recursivity.
  CALL SET OUTPUT_DIR=%%INPUT_DIR:INPUT=!DATA_OUTPUT!%%

  REM Check if the output directory exist and create it if necessary.
  IF NOT EXIST "!OUTPUT_DIR!" MD "!OUTPUT_DIR!"

  REM Set the actual path (path and filename with fileending).
  SET ACTUAL_PATH=%%F

  REM Set the output path (path and filename with fileending).
  SET OUTPUT_PATH=!OUTPUT_DIR!%%~nF.!NEW_FILE_EXTENSION!

  REM Check if the input file size is not zero.
  IF !SIZE_BEGIN_B! NEQ 0 (

    REM Check if the optimizing method is 'Guetzli'.
    IF !METHOD! EQU 1 (

      REM Execute the 'guetzli.exe' to optimize the actual image.
      "!EXEC_DIR!\guetzli.exe" --nomemlimit --quality !QUALITY! "%%F" "!OUTPUT_PATH!"
    )

    REM Check if the optimizing method is 'Mozjpeg'.
    IF !METHOD! EQU 2 (

      REM Execute the 'cjpeg.exe' to optimize the actual image.
      "!EXEC_DIR!\cjpeg.exe" -quality !QUALITY! "%%F" > "!OUTPUT_PATH!"
    )

    REM Calculate the filesize for the actual file - After optimizing.
    FOR /F "usebackq delims=" %%A IN ('!OUTPUT_PATH!') DO set SIZE_END_B=%%~zA

    REM The input file size is zero.
  ) ELSE (

    REM Set the end size of the file.
    SET SIZE_END_B=0
  )

  REM Display an information message - Output file.
  ECHO  !FILES_TOTAL! - !OUTPUT_FILE_EXTENSION! - Saving output file:    !OUTPUT_PATH! ( !SIZE_END_B! B ^^^)...

  REM Check if the input file size is not zero.
  IF !SIZE_END_B! NEQ 0 (

    REM Calculate the size difference of the file.
    SET /a SIZE_DIFF_B=!SIZE_BEGIN_B!-!SIZE_END_B!
    SET /a SIZE_DIFF_P=!SIZE_DIFF_B!*100/!SIZE_BEGIN_B!

    REM The input file size is zero.
  ) else (

    REM Set the size difference of the file.
    SET SIZE_DIFF_B=0
    SET SIZE_DIFF_P=0
  )


  REM ==================================================================
  REM     C H E C K   I F   F I L E S I Z E   I S   Z E R O
  REM ==================================================================

  REM Check if the optimization failed and the output file size is zero.
  IF !SIZE_END_B! EQU 0 (

    REM Increase the error counter.
    SET /a ERROR_COUNTER=!ERROR_COUNTER!+1

    REM Check if the 'cecho.exe' executable exists.
    IF EXIST "!EXEC_DIR!\cecho.exe" (

      REM Display an error message for the actual file - Using CECHO.
      "!EXEC_DIR!\cecho.exe" {black on red}[ Optimization FAILED^^^! ]{18} {black on red}[ !SIZE_DIFF_P! %% ]{18} {black on red}[ !SIZE_DIFF_B! B ]{18} {black on red}[ The output file size is zero^^^! =^^^> Copy file... ]{18}{\n}{\n}

      REM The 'cecho.exe' executable does not exist.
    ) ELSE (

      REM Display an error message - Using ECHO.
      ECHO [ Optimization FAILED^^^! ] [ !SIZE_DIFF_P! %% ] [ !SIZE_DIFF_B! B ] [ The output file size is zero^^^! =^^^> Copy file... ]!\n!
    )

    REM Check if the logging is enabled.
    IF %LOG_ERRORS%==1 (

      REM Write the error to a logfile.
      ECHO [ The output file size is zero: !SIZE_DIFF_P! %% ^^^| !SIZE_DIFF_B! B ] %%F >> "!EXEC_DIR!\error.log"
    )
  )


  REM ==================================================================
  REM     C H E C K   I F   D I F F E R E N C E   I S   H I G H
  REM ==================================================================

  REM Check if the error counter is zero.
  IF !ERROR_COUNTER! EQU 0 (

    REM Check if the optimization failed and the file size difference is
    REM exteremely high >= maximum optimization.
    IF !SIZE_DIFF_P! GEQ !MAX_OPTIMIZATION! (

      REM Increase the error counter.
      SET /a ERROR_COUNTER=!ERROR_COUNTER!+1

      REM Check if the 'cecho.exe' executable exists.
      IF EXIST "!EXEC_DIR!\cecho.exe" (

        REM Display an error message for the actual file - Using CECHO.
        "!EXEC_DIR!\cecho.exe" {black on red}[ Optimization FAILED^^^! ]{18} {black on red}[ !SIZE_DIFF_P! %% ]{18} {black on red}[ !SIZE_DIFF_B! B ]{18} {black on red}[ The file size difference is very high^^^! =^^^> Copy file... ]{18}{\n}{\n}

        REM The 'cecho.exe' executable does not exist.
      ) ELSE (

        REM Display an error message - Using ECHO.
        ECHO [ Optimization FAILED^^^! ] [ !SIZE_DIFF_P! %% ] [ !SIZE_DIFF_B! B ] [ The file size difference is very high^^^! =^^^> Copy file... ]!\n!
      )

      REM Check if the logging is enabled.
      IF %LOG_ERRORS%==1 (

        REM Write the error to a logfile.
        ECHO [ File size difference is very high: !SIZE_DIFF_P! %% ^^^| !SIZE_DIFF_B! B ] %%F >> "!EXEC_DIR!\error.log"
      )
    )


    REM ================================================================
    REM     C H E C K   O P T I M I Z A T I O N   M I N I M U M
    REM ================================================================

    REM Check if the error counter is zero.
    IF !ERROR_COUNTER! EQU 0 (

      REM Check if the optimization failed and the output file is equal or
      REM bigger than the input file <= minimum optimization.
      IF !SIZE_DIFF_P! LEQ !MIN_OPTIMIZATION! (

        REM Increase the error counter.
        SET /a ERROR_COUNTER=!ERROR_COUNTER!+1

        REM Check if the 'cecho.exe' executable exists.
        IF EXIST "!EXEC_DIR!\cecho.exe" (

          REM Display an error message - Using CECHO.
          "!EXEC_DIR!\cecho.exe" {black on red}[ Optimization FAILED^^^! ]{18} {black on red}[ !SIZE_DIFF_P! %% ]{18} {black on red}[ !SIZE_DIFF_B! B ]{18} {black on red}[ Optimization not possible^^^! =^^^> Copy file... ]{18}{\n}{\n}

          REM The 'cecho.exe' executable does not exist.
        ) ELSE (

          REM Display an error message - Using ECHO.
          ECHO [ Optimization FAILED^^^! ] [ !SIZE_DIFF_P! %% ] [ !SIZE_DIFF_B! B ] [ Optimization not possible^^^! =^^^> Copy file... ]!\n!
        )

        REM Check if the logging is enabled.
        IF %LOG_ERRORS%==1 (

          REM Write the error to a logfile.
          ECHO [ Optimization not possible: !SIZE_DIFF_P! %% ^^^| !SIZE_DIFF_B! B ] %%F >> "!EXEC_DIR!\error.log"
        )


        REM ============================================================
        REM     O P T I M I Z A T I O N   S U C C E S S F U L
        REM ============================================================

        REM The optimization was successful.
      ) ELSE (

        REM Increase the optimized file counter.
        SET /a FILES_OPTIMIZED=!FILES_OPTIMIZED!+1

        REM Calculate the total size of the files.
        SET /a TOTAL_SIZE_BEGIN_B=!TOTAL_SIZE_BEGIN_B!+!SIZE_BEGIN_B!
        SET /a TOTAL_SIZE_END_B=!TOTAL_SIZE_END_B!+!SIZE_END_B!

        REM Calculate the temporary size difference over all processed files.
        SET /a TOTAL_SIZE_DIFF_B=!TOTAL_SIZE_BEGIN_B!-!TOTAL_SIZE_END_B!

        REM Check if the integer limit of 2147483647 is near. The size
        REM difference should be a maximum of 21474836, because multi-
        REM plied by 100 it will not hit the limit.
        IF !TOTAL_SIZE_DIFF_B! GEQ 21474836 (

          REM Calculate the temporary values in KB to reduce the size
          REM of the integers for the percent calculation.
          SET /a TEMP_TOTAL_SIZE_DIFF_B=!TOTAL_SIZE_DIFF_B!/1024
          SET /a TEMP_TOTAL_SIZE_BEGIN_B=!TOTAL_SIZE_BEGIN_B!/1024

          REM Calculate the size difference in percent.
          SET /a TEMP_SIZE_DIFF_P=!TEMP_TOTAL_SIZE_DIFF_B!*100/!TEMP_TOTAL_SIZE_BEGIN_B!

          REM The integer limit of 2147483647 is not near.
        ) ELSE (

          REM Calculate the size difference in percent.
          SET /a TEMP_SIZE_DIFF_P=!TOTAL_SIZE_DIFF_B!*100/!TOTAL_SIZE_BEGIN_B!
        )

        REM Check if the 'cecho.exe' executable exists.
        IF EXIST "!EXEC_DIR!\cecho.exe" (

          REM Display the filesize for the actual file - Using CECHO.
          "!EXEC_DIR!\cecho.exe" {green on lime}[ Optimization SUCCESSFUL^^^! ]{18} {green on lime}[ !SIZE_DIFF_P! %% ]{18} {green on lime}[ !SIZE_DIFF_B! B ]{18} {97}[ Total: !TEMP_SIZE_DIFF_P! %% ]{18} {97}[ Total: !TOTAL_SIZE_DIFF_B! B ]{18}{\n}{\n}

          REM The 'cecho.exe' executable does not exist.
        ) ELSE (

          REM Display the filesize for the actual file - Using ECHO.
          ECHO [ Optimization SUCCESSFUL^^^! ] [ !SIZE_DIFF_P! %% ] [ !SIZE_DIFF_B! B ] [ Total: !TEMP_SIZE_DIFF_P! %% ] [ Total: !TOTAL_SIZE_DIFF_B! B ]!\n!
        )
      )
    )
  )

  REM Check if the error counter is not zero.
  IF !ERROR_COUNTER! GEQ 1 (


    REM ================================================================
    REM     D E L E T E   U N U S E D   O U T P U T   F I L E
    REM ================================================================

    REM Increase the failed file counter.
    SET /a FILES_FAILED=!FILES_FAILED!+1

    REM Remove the output file if necessary.
    IF EXIST "!OUTPUT_PATH!" ECHO j | DEL "!OUTPUT_PATH!" > NUL
    IF EXIST "!OUTPUT_PATH!" ECHO y | DEL "!OUTPUT_PATH!" > NUL


    REM ================================================================
    REM     C O P Y   O R I G I N A L   F I L E   T O   O U T P U T
    REM ================================================================

    REM Set the output path (path and filename with fileending).
    SET COPY_PATH=!OUTPUT_DIR!%%~nxF

    REM Copy the input file to the output directory.
    COPY /Y "%%F" "!COPY_PATH!" > NUL
  )
)


REM ====================================================================
REM     T O T A L   S I Z E   C A L C U L A T I O N
REM ====================================================================

REM Calculate the size difference.
SET /a TOTAL_SIZE_DIFF_B=!TOTAL_SIZE_BEGIN_B!-!TOTAL_SIZE_END_B!

REM Check if the integer limit of 2147483647 is near.
IF !TOTAL_SIZE_DIFF_B! GEQ 21474836 (

  REM Calculate the total values in KB to reduce the size of the
  REM integers for the percent calculation.
  SET /a TOTAL_SIZE_DIFF_KB=!TOTAL_SIZE_DIFF_B!/1024
  SET /a TOTAL_SIZE_BEGIN_KB=!TOTAL_SIZE_BEGIN_B!/1024

  REM Calculate the size difference in percent.
  SET /a TOTAL_SIZE_DIFF_P=!TOTAL_SIZE_DIFF_KB!*100/!TOTAL_SIZE_BEGIN_KB!

  REM The integer limit of 2147483647 is not near.
) ELSE (

  REM Calculate the size difference in percent.
  SET /a TOTAL_SIZE_DIFF_P=!TOTAL_SIZE_DIFF_B!*100/!TOTAL_SIZE_BEGIN_B!
)

REM Calculate the total end size in percent.
SET /a TOTAL_SIZE_END_P=100-!TOTAL_SIZE_DIFF_P!


REM ====================================================================
REM     T O T A L   F I L E   N U M B E R   C A L C U L A T I O N
REM ====================================================================

REM Calculate the percentage of files.
SET /a TOTAL_OPTIMIZED_P=!FILES_OPTIMIZED!*100/!FILES_TOTAL!
SET /a TOTAL_FAILED_P=!FILES_FAILED!*100/!FILES_TOTAL!


REM ====================================================================
REM     D I S P L A Y   R E S U L T
REM ====================================================================

REM Display the optimization result header.
ECHO --------------------------------------------------------------------------------
ECHO  Optimizing Results [ Method: %METHOD% ^| Quality: %QUALITY% ^| Minimum: %MIN_OPTIMIZATION% %% ^| Maximum: %MAX_OPTIMIZATION% %% ]
ECHO --------------------------------------------------------------------------------
ECHO.
ECHO  Image files optimized: !FILES_OPTIMIZED! [ %TOTAL_OPTIMIZED_P% %% ]
ECHO  Image files failed:    !FILES_FAILED! [ %TOTAL_FAILED_P% %% ]
ECHO  Image files total:     !FILES_TOTAL! [ 100 %% ]
ECHO.
ECHO --------------------------------------------------------------------------------
ECHO.
ECHO  Total input size:      !TOTAL_SIZE_BEGIN_B! B [ 100 %% ]
ECHO  Total output size:     !TOTAL_SIZE_END_B! B [ !TOTAL_SIZE_END_P! %% ]

REM Check if the 'cecho.exe' executable exists.
IF EXIST "!EXEC_DIR!\cecho.exe" (

  REM Display the total difference - Using CECHO.
  "!EXEC_DIR!\cecho.exe" {green on lime} Total difference:      %TOTAL_SIZE_DIFF_B% B [ %TOTAL_SIZE_DIFF_P% %% ]{18}{\n}

  REM The 'cecho.exe' executable does not exist.
) ELSE (

  REM Display the total difference - Using ECHO.
  ECHO  Total difference:      %TOTAL_SIZE_DIFF_B% B [ %TOTAL_SIZE_DIFF_P% %% ]
)

ECHO.
ECHO --------------------------------------------------------------------------------
ECHO.
ENDLOCAL

REM Pause the script.
PAUSE

REM Exit the script.
EXIT /b
